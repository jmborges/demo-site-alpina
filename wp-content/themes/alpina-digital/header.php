<!DOCTYPE html>
<html <?php language_attributes();  ?>>

<head>

    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>
        <?php
        wp_title('|', true, 'right');
        bloginfo('name');
        ?>
    </title>

    <meta property="og:type" content="website" />
    <meta property="og:image" content="<?php echo get_template_directory_uri() . '/assets/images/og_image.png' ?>" />
    <meta property="og:title" content="<?php bloginfo('name'); ?>" />
    <meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
    <meta name="description" content="Geramos valor para o seu negócio através de soluções digitais. Site, Ecommerce, Marketing Digital e Marketing de Performance. Passo Fundo. Porto Alegre.">
    <meta property="og:description" content="Geramos valor para o seu negócio através de soluções digitais. Site, Ecommerce, Marketing Digital e Marketing de Performance. Passo Fundo. Porto Alegre." />

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
    <main>

        <div class="mobile-menu" style="display: none;">
            <div class="row justify-content-between text-center mt-4 mb-4">
                <div class="col-6">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/logo-preto-alpina.png' ?>" />
                </div>
                <div class="col-6">
                    <div class="close-mobile-menu">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/close-mobile-menu.png' ?>" alt="">
                    </div>
                </div>
            </div>

            <ul style="font-size: 18px">
                <li class="mt-4">
                    Início
                </li>
                <li class="mt-4">
                    Sobre
                </li>
                <li class="mt-4">
                    Soluções
                </li>
                <li class="mt-4">
                    Cases
                </li>
                <li class="mt-4">
                    Contato
                </li>
            </ul>

            <div class="col-12 col-2 text-center">
                <div class="mt-3">
                    <img class="m-2" style="cursor: pointer;" src="<?php echo get_template_directory_uri() . '/assets/images/ig-preto.png' ?>" alt="">
                    <img class="m-2" style="cursor: pointer;" src="<?php echo get_template_directory_uri() . '/assets/images/linkedin-preto.png' ?>" alt="">
                    <img class="m-2" style="cursor: pointer;" src="<?php echo get_template_directory_uri() . '/assets/images/whatsapp-preto.png' ?>" alt="">
                    <img class="m-2" style="cursor: pointer;" src="<?php echo get_template_directory_uri() . '/assets/images/youtube-preto.png' ?>" alt="">
                </div>
            </div>

        </div>

        <header class="site-header">
            <div class="row justify-content-between pt-3 pb-3">
                <div class="col-6 col-sm-3 text-center logo-top">
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/logo-branco-alpina.png' ?>" />
                    </div>
                </div>
                <div class="col-6 col-sm-3 mobile-toggler">
                    <button class="me-2 navbar-toggler navbar-mobile-button" type="button" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <div class="hamburguer">
                            <div class="lines line-top ">
                            </div>
                            <div class="lines line-mid ">
                            </div>
                            <div class="lines line-mid ">
                            </div>
                            <div class="lines line-bottom ">
                            </div>
                        </div>
                    </button>
                </div>
                <div class="col-6 col-sm-6 text-center desktop-menu">
                    <ul style="font-size: 18px; padding-top: 1%" class="horizontal-list">
                        <li class="active-menu-item">
                            <a href="#" class="active-menu-link">
                                Início
                            </a>
                        </li>
                        <li class="ms-5">
                            <a href="#">
                                Sobre
                            </a>
                        </li>
                        <li class="ms-5 dropdown">
                            <a class="dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Soluções
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item tech-dropdown-item" href="#">Tecnologia</a></li>
                                <li><a class="dropdown-item mkt-dropdown-item" href="#">Marketing Digital</a></li>
                            </ul>
                        </li>
                        <li class="ms-5">
                            <a href="#">
                                Cases
                            </a>
                        </li>
                        <li class="ms-5">
                            <a href="#">
                                Contato
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-3 header-right-side">
                    <div class="row">
                        <div class="col text-center pt-3">
                            <span class="dropdown">
                                <a class="nav-link dropdown-toggle" style="text-align: right;" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    PT
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li><a class="dropdown-item" href="#"></a></li>
                                    <li><a class="dropdown-item" href="#"></a></li>
                                    <li><a class="dropdown-item" href="#"></a></li>
                                </ul>
                            </span>
                        </div>
                        <div class="col">
                            <button class="std-btn">
                                Solicitar proposta
                            </button>
                        </div>
                    </div>
                </div>
        </header>