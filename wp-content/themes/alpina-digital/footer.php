<footer class="footer" style="padding-top: 3%;padding-bottom: 2%">

    <div class="container">
        <div class="row justify-content-between mb-5">
            <div class="col-12 col-sm-2 text-center">
                <div class="m-3">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/logo-min.png' ?>" alt="">
                </div>
            </div>
            <div class="col-12 col-sm-2 text-center">
                <div class="mt-3">
                    <img class="m-2" style="cursor: pointer;" src="<?php echo get_template_directory_uri() . '/assets/images/ig.png' ?>" alt="">
                    <img class="m-2" style="cursor: pointer;" src="<?php echo get_template_directory_uri() . '/assets/images/linkedin.png' ?>" alt="">
                    <img class="m-2" style="cursor: pointer;" src="<?php echo get_template_directory_uri() . '/assets/images/whatsapp.png' ?>" alt="">
                    <img class="m-2" style="cursor: pointer;" src="<?php echo get_template_directory_uri() . '/assets/images/youtube.png' ?>" alt="">
                </div>
            </div>
        </div>

        <hr>

        <div class="row justify-content-between mt-5">

            <div class="col-12 col-sm-2">
                <ul class="footer-ul std-text">
                    <li>
                        <a href="#">
                            Início
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Sobre
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Soluções
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Tecnologia
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Marketing Digital
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Cases
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Aprenda
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Contato
                        </a>
                    </li>
                </ul>

            </div>
            <div class="col-12 col-sm-2">
                <ul class="footer-ul std-text">
                    <li>
                        <a href="#">
                            Suporte
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Chamados
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            E-mail
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Chat Online
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-12 col-sm-3 text-center">
                <b>Atendimento</b>
                <ul class="footer-ul std-text">
                    <li>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/whatsapp.png' ?>" alt=""> (54) 98358-4713
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/phone.png' ?>" alt=""> (54) 9999-9999
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-12 col-sm-5">
                <b>ALP NEWS</b>
                <div class="std-text">
                    Assine nossa newsletter e receba insights sobre Tecnologia, Marketing e Negócios
                </div>
                <div class="mt-3">
                    <input class="grey-input" type="text" placeholder="E-mail">
                </div>
                <div class="mt-3">
                    <button class="white-btn">
                        Quero receber os insights
                    </button>
                </div>
            </div>
        </div>

        <hr>

        <div class="row justify-content-between mt-5">
            <div class="col-12 col-sm-2" style="font-size: 0.9em;color: grey">

                <div>
                    Encarregado dos dados
                </div>
                <div>
                    Fabrício Raimondi
                </div>
                <div>
                    <a href="mailto:privacidade@alpina.digital">
                        privacidade@alpina.digital
                    </a>
                </div>
            </div>
            <div class="col-12 col-sm-4" style="font-size: 0.9em;color: grey">
                <div class="row mt-5">
                    <div class="col">
                        <a href="#">
                            Termos de uso
                        </a>
                    </div>
                    <div class="col">
                        <a href="#">
                            Política de privacidade
                        </a>
                    </div>
                </div>


            </div>
        </div>

    </div>

</footer>

</main>
</body>

</html>