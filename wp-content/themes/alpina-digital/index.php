<?php 
    get_header();
?>
    
<?php 
get_template_part( 'template-parts/section', 'head' );
?>
   
<?php 
get_template_part( 'template-parts/section', 'solutions' );
?>

<?php 
get_template_part( 'template-parts/section', 'what-is' );
?>

<?php 
get_template_part( 'template-parts/section', 'clients' );
?>

<?php 
get_template_part( 'template-parts/section', 'form' );
?>


<?php 
get_footer();
?>