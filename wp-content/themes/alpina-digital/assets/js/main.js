$(function () {
    /* $('#leftBtn').on('click', function () {
        $('#banner-slider').animate({
            marginLeft: '-1728px'
        });

    });

    $('#rightBtn').on('click', function () {
        $('#banner-slider').animate({
            marginLeft: '0px'
        });
    }); */

    $('.rightBtn').on('click', function () {
        $('.version-one').animate({
            marginLeft: '-1900px',
        });
        $('.version-two').animate({
            left: '0px'
        });

    });
    $('.leftBtn').on('click', function () {
        $('.version-one').animate({
            marginLeft: '0',
        });
        $('.version-two').animate({
            left: '100%'
        });

    });

    $('.rightBtnMobile').on('click', function () {
        $('.version-one').animate({
            marginLeft: '-1000px',
        });
        $('.version-two').animate({
            left: '0px'
        });

    });
    $('.leftBtnMobile').on('click', function () {
        $('.version-one').animate({
            marginLeft: '0',
        });
        $('.version-two').animate({
            left: '100%'
        });

    });

    $('.navbar-mobile-button').on('click', function(){
        $('.mobile-menu').slideToggle()
    })

    $('.close-mobile-menu').on('click', function(){
        $('.mobile-menu').slideUp()
    })

    $('.tech').on('mouseenter', function(){
        $('.solutions-tech-icon-dark').show()
        $('.solutions-tech-icon-white').hide()
    })

    $('.tech').on('mouseleave', function(){
        $('.solutions-tech-icon-dark').hide()
        $('.solutions-tech-icon-white').show()
    })
    
});
