<?php

function alpina_theme_enqueue_external(){
     wp_enqueue_style('alpina-bootstrap-css-handle', 'https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css' );
    wp_enqueue_script('alpina-bootstrap-js-handle', 'https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js');
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Roboto&display=swap');
    wp_enqueue_script('alpina-jquery-handle', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js');
    wp_enqueue_script('alpina-carousel-handle', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js');
}

function alpina_theme_enqueue_styles(){
    wp_enqueue_style('alpina-theme-main-style', get_template_directory_uri() . '/assets/css/main.css' , [], rand(100000,999999999));
    wp_enqueue_script('alpina-theme-carousel', get_template_directory_uri() . '/assets/js/carousel.js' , [], rand(100000,999999999));
    wp_enqueue_script('alpina-theme-main-js', get_template_directory_uri() . '/assets/js/main.js' , [], rand(100000,999999999));
}

add_action('wp_enqueue_scripts','alpina_theme_enqueue_external');
add_action('wp_enqueue_scripts', 'alpina_theme_enqueue_styles');

