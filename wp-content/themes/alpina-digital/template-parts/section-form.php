<section class="form-section">

    <div class="container">

        <div class="row " style="padding-top: 20%;">
            <div class="col-12 col-sm-6">
                <div>
                    <h2 class="form-title">
                        JÁ PENSOU EM FAZER PARTE DO NOSSO TIME DE ALPINISTAS?
                    </h2>

                    <div>
                        <div class="mt-3">
                            <label class="std-label">Nome</label>
                            <input class="std-input" type="text" placeholder="Seu nome completo">
                        </div>
                        <div class="mt-3">
                            <label class="std-label">E-mail</label>
                            <input class="std-input" type="text" placeholder="Seu e-mail">
                        </div>
                        <div class="mt-3">
                            <label class="std-label"> Telefone</label>
                            <input class="std-input" type="text" placeholder="Seu telefone com código de área">
                        </div>
                        <div class="mt-4">
                            <label class="std-label">Mensagem</label>
                            <textarea class="std-input " name="" id="" placeholder="Mensagem"></textarea>
                        </div>

                        <div class="mt-4">
                            <input type="file" id="file" name="file" class="attach-btn" />
                            <label for="file" class="file-label text-center">Anexar currículo <img src="<?php echo get_template_directory_uri() . '/assets/images/arrow.png' ?>" alt=""></label>
                        </div>

                        <div class="mt-4">
                            <button class="std-btn">
                            Quero ser alpinista
                            </button>
                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>


</section>