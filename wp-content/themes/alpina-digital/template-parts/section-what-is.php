<section class="section-what-is">

    <div class="container">

        <div class="mb-5">
            <div class="row justify-content-center">
                <div class="col-11 col-sm-5">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/foto-alp-1.png' ?>" alt="">
                </div>

                <div class="col-12 col-sm-5 what-is-first-text-container">
                    <div class="std-text what-is-first-text">
                        Alpinistas são pessoas que estão <span style="color: white">aprendendo o tempo todo.</span> Buscam estar sempre preparados para avançar na velocidade das transformações.
                        <span style="color: white">Não temem mudanças, pelo contrário, são encorajados por elas,</span> sabe a chamada "zona de desconforto"? Para um alpinista, esse é o seu habitat.
                    </div>
                </div>
            </div>

            <div class="relative-title">
                <h2 class="alpinista-title" style="font-weight: 200">
                    O QUE É <span style="font-weight: 800">SER</span>
                    <br>
                    <span style="font-weight: 800">UM ALPINISTA?</span>
                </h2>
            </div>

            <div class="selo">
            <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/selo.png' ?>" alt="">
            </div>

            <div class="relative-title-mobile">
                <h2 class="alpinista-title" style="font-weight: 200">
                    O QUE É <span style="font-weight: 800">SER</span>
                    <br>
                    <span style="font-weight: 800">UM ALPINISTA?</span>
                </h2>
            </div>
        </div>

        <div class="mt-5">
            <div class="row">
                <div class="col-sm-6 alp-photo-2-mobile">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/assets/images/foto-alp-2.png' ?>" alt="">
                </div>
                <div class="col-sm-5" style="padding-top: 15%;">
                    <div class="ms-3">
                        <h2 class="alpinista-title mb-3" style="font-weight: 800;">
                            OS ALPINISTAS
                        </h2>
                        <div class="std-text what-is-second-text">
                            Alpinistas são pessoas que estão <span style="color: white">aprendendo o tempo todo.</span> Buscam estar sempre preparados para avançar na velocidade das transformações.
                            <span style="color: white">Não temem mudanças, pelo contrário, são encorajados por elas,</span> sabe a chamada "zona de desconforto"? Para um alpinista, esse é o seu habitat.
                        </div>
                        <div style="padding-top: 15%;">
                            <button class="std-btn">
                                Quero ser um alpinista
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 alp-photo-2-desktop">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/foto-alp-2.png' ?>" alt="">
                </div>
            </div>

        </div>

    </div>

</section>