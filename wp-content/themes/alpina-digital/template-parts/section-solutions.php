<section id="section-solutions">

    <div class="container">

        <div class="solutions-text">
            <div class="row justify-content-between">
                <div class="col-12 col-sm-3">
                    <h2 class="solutions-title">
                        SOLUÇÕES
                    </h2>
                </div>
                <div class="col-12 col-sm-4">
                    <p class="std-text">
                        Nós queremos ser o seu parceiro estratégico.
                        <br>
                        Saiba o que podemos fazer <b style="color: white">pelo seu negócio</b>.
                    </p>
                </div>

                <div class="col-12 col-sm-3">
                    <div class="proposal-text-container">
                        <span class="std-text-white">Solicitar proposta</span>
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/arrow.png' ?>" alt="">
                    </div>
                </div>
            </div>
        </div>

        <?php

        $args = [
            'post_type'      => 'solutions',
            'posts_per_page' => 2,
        ];

        $loop = new WP_Query($args);

        ?>

        <div class="row mt-5">

            <?php while ($loop->have_posts()) : ?>

                <?php
                $loop->the_post();
                ?>

                <div class="col-12 col-sm-6  ">
                    <div class="card-solutions tech" style="font-size: 18px;">
                        <div class="arrow-solutions-desktop" style="background-color: black;width: 50px">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/arrow.png' ?>" alt="">
                        </div>
                        <div class="solutions-tech-icon-dark" style="display: none;">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/tech-icon.png' ?>" alt="">
                        </div>
                        <div class="solutions-tech-icon-white">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/tech-icon-white.png' ?>" alt="">
                        </div>
                        <div style="font-weight: 800;font-size: 32px" class="mt-3 mb-4">
                            <?php the_title(); ?>
                        </div>
                        <?php the_content(); ?>

                    </div>
                </div>

            <?php endwhile; ?>

        </div>


    </div>

</section>