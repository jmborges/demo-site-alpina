<section class="clients-section" style="margin-top: 5%; padding: 1%">

    <div class="text-center pt-5 mb-4">
        Não é sobre chegar ao topo,
        <br>
        é sobre como e <b>com quem você chega lá.</b>
    </div>

    <?php

    $args = [
        'post_type'      => 'clients',
    ];

    $loop = new WP_Query($args);

    ?>

    <section class="customer-logos slider text-center">

        <?php while ($loop->have_posts()) : ?>

            <?php
            $loop->the_post();
            ?>

            <div class=" slide logo-desktop logo-mobile">
                <img class="img-fluid" src="<?php the_post_thumbnail_url() ?>" alt="">
            </div>

        <?php endwhile; ?>

    </section>


</section>