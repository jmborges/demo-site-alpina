<div class="relative-div mt-5">
    <div class="section-head version-one">
        <div class="container">
            <div class="head-content">
                <div class="row align-items-center">
                    <div class="col-11 col-sm-6">
                        <div class="container-teste">
                            <div class="one">
                                <h1 class="main-title">
                                    <span style="font-weight: 100;">ESTÚDIO DE DESIGN</span>
                                    <span style="font-weight: bolder;"><b>COM DNA DIGITAL.</b></span>
                                </h1>
                                <div class="mt-4 std-text">
                                    Somos <span style="color: white">especialistas em gerar valor</span> ao seu negócio por meio de soluções de <span style="color: white">tecnologia e marketing digital</span>
                                </div>

                                <div class="mt-4" style="font-size: 24px;cursor: pointer">
                                    Ver nossos cases <img src="<?php echo get_template_directory_uri() . '/assets/images/arrow.png' ?>" alt="">
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="arrows-desktop" >
                <span class="me-3">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/left-arrow.png' ?>" alt="">
                </span>
                <span class="rightBtn">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/right-arrow.png' ?>" alt="">
                </span>
            </div>

            <!-- <div class="arrows-mobile">
                <div>
                    <img src="<?php /* echo get_template_directory_uri() . '/assets/images/left-arrow.png' */ ?>" alt="">
                </div>
                <div class="rightBtnMobile">
                    <img src="<?php /* echo get_template_directory_uri() . '/assets/images/right-arrow.png' */ ?>" alt="">
                </div>
            </div> -->

            <div class="section-head-mobile-one">
            </div>

        </div>
    </div>

    <div class="section-head version-two">

        <div class="container">

            <div class="head-content">
                <div class="row align-items-center">
                    <div class="col-11 col-sm-6">
                        <div class="container-teste">
                            <div class="one">
                                <h1 class="main-title">
                                    <span style="font-weight: 100;">UM ESTÚDIO INDEPENDENTE DE</span>
                                    <span style="font-weight: bolder;"><b>DESIGN ESTRATÉGICO</b></span>
                                </h1>
                                <div class="mt-4 std-text">
                                    <!-- Somos <span style="color: white">especialistas em gerar valor</span> ao seu negócio por meio de soluções de <span style="color: white">tecnologia e marketing digital</span> -->
                                    Conectamos pessoas com diferentes skills e um objetivo:
                                    <br>
                                    Gerar valor para o seu negócio. Colocando as <span style="color: white">pessoas no centro da experiência digital.</span>
                                </div>

                                <div class="mt-4" style="font-size: 24px; cursor: pointer">
                                    Ver nossos cases <img src="<?php echo get_template_directory_uri() . '/assets/images/arrow.png' ?>" alt="">
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="arrows-desktop">
                <span class="me-3 leftBtn">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/left-arrow.png' ?>" alt="">
                </span>
                <span >
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/right-arrow.png' ?>" alt="">
                </span>
            </div>

            <!-- <div class="arrows-mobile">
                <div class="leftBtnMobile">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/left-arrow.png' ?>" alt="">
                </div>
                <div>
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/right-arrow.png' ?>" alt="">
                </div>
            </div> -->

            <!-- <div class="section-head-mobile-two">
            </div> -->

        </div>
    </div>
</div>