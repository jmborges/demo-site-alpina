<?php

require get_template_directory() . '/inc/enqueue_assets.php';

add_theme_support( 'post-thumbnails' );

function remove_admin_login_header()
{
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');

function create_solutions_post_type()
{
    register_post_type(
        'solutions',
        [
            'labels'    => [
                'name'          => __('Soluções'),
                'singular_name' => __('Solução'),
            ],
            'public'      => true,
        ]
    );
}
add_action('init', 'create_solutions_post_type');

function create_clients_post_type()
{
    register_post_type(
        'clients',
        [
            'labels'    => [
                'name'          => __('Clientes'),
                'singular_name' => __('Cliente'),
            ],
            'public'      => true,
            'supports' => array( 'title', 'thumbnail' ),
        ]
    );
}
add_action('init', 'create_clients_post_type');

